FROM ubuntu

RUN apt-get update && \
    apt-get install curl make libgmp-dev libffi6 -y && \
    curl -O https://44a95588fe4cc47efd96-ec3c2a753a12d2be9f23ba16873acc23.ssl.cf2.rackcdn.com/linux-64.tar.gz && \
    apt-get purge -y --auto-remove curl && \
    tar zxf linux-64.tar.gz && \
    chmod +x elm && \
    cp elm /usr/local/bin

ENTRYPOINT "elm"
