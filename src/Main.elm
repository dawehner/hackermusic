module Main exposing (..)

import Browser
import Element as E
import Element.Events as EE
import Html
import Html.Attributes


type Content
    = Youtube
        { label : String
        , embed : String
        }
    | Spotify
        { label : String
        , embed : String
        }


type alias Model =
    { activeContent : Maybe Content
    , availableContent : List Content
    }


init flags =
    ( { activeContent = Nothing
      , availableContent =
            [ Youtube
                { label = "The Prodigy - Breathe"
                , embed = "rmHDhAohJlQ"
                }
            , Youtube
                { label = "Kraftwerk - Numbers"
                , embed = "M3f-JwOGO1A"
                }
            , Spotify
                { label = "Hackers coffee playlist"
                , embed = "user/omegak/playlist/4mWcjE2mVcaALYi6v8hDZN"
                }
            ]
      }
    , Cmd.none
    )


subscriptions =
    Sub.none


type Msg
    = SelectContent Content
    | UnSelectContent


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SelectContent content ->
            ( { model | activeContent = Just content }
            , Cmd.none
            )

        UnSelectContent ->
            ( { model | activeContent = Nothing }, Cmd.none )


view model =
    { title = "Hackermusic <3"
    , body =
        [ doView model
        ]
    }


viewcontentList : List Content -> E.Element Msg
viewcontentList contents =
    List.map
        (\content ->
            case content of
                Youtube { label } ->
                    E.el [ EE.onClick (SelectContent content) ] (E.text label)

                Spotify { label } ->
                    E.el [ EE.onClick (SelectContent content) ] (E.text label)
        )
        contents
        |> E.column [ E.centerY, E.height E.fill ]


viewContent : Content -> E.Element Msg
viewContent content =
    E.column []
        [ case content of
            Youtube { embed } ->
                E.html
                    (Html.node "iframe"
                        [ Html.Attributes.attribute "width" "560"
                        , Html.Attributes.attribute "height" "315"
                        , Html.Attributes.attribute "src" ("//www.youtube.com/embed/" ++ embed ++ "?autoplay=1")
                        , Html.Attributes.attribute "frameborder" "0"
                        , Html.Attributes.attribute "allow" "autoplay; encrypted-media"
                        , Html.Attributes.attribute "allowfullscreen" "1"
                        ]
                        []
                    )

            Spotify { embed } ->
                E.html
                    (Html.node "iframe"
                        [ Html.Attributes.attribute "width" "560"
                        , Html.Attributes.attribute "height" "315"
                        , Html.Attributes.attribute "src" ("//open.spotify.com/embed/" ++ embed ++ "?autoplay=1")
                        , Html.Attributes.attribute "frameborder" "0"
                        , Html.Attributes.attribute "allow" "autoplay; encrypted-media"
                        , Html.Attributes.attribute "allowfullscreen" "1"
                        ]
                        []
                    )
        , E.el [ EE.onClick UnSelectContent ] (E.text "Close")
        ]


doView model =
    E.layout [ E.height E.fill ]
        (case model.activeContent of
            Just content ->
                E.row []
                    [ E.el [ E.width (E.fillPortion 1) ] (viewcontentList model.availableContent)
                    , E.el [ E.width (E.fillPortion 2) ] (viewContent content)
                    ]

            Nothing ->
                E.row []
                    [ E.el [ E.width (E.fillPortion 1) ] (viewcontentList model.availableContent)
                    ]
        )


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , subscriptions = \_ -> subscriptions
        , update = update
        , view = view
        }
