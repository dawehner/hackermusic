build:
	elm make src/Main.elm --output=public/index.html

dev:
	elm reactor --port=8124

.PHONY: build dev
